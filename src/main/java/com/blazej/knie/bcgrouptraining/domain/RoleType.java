package com.blazej.knie.bcgrouptraining.domain;

import lombok.Getter;

@Getter
public enum RoleType {
    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER");

    private final String roleAsString;
    RoleType(String roleAsString) {
        this.roleAsString = roleAsString;
    }
}
