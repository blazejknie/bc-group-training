package com.blazej.knie.bcgrouptraining.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class CryptoCurrency extends BaseEntity {
    private String name;
    private BigDecimal valueInUSD;

    @Builder
    public CryptoCurrency(String name, BigDecimal valueInUSD) {
        super();
        this.name = name;
        this.valueInUSD = valueInUSD;
    }
}
