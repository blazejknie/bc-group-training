package com.blazej.knie.bcgrouptraining.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Account extends BaseEntity{
    private String accountNumber;

    @ManyToOne
    @JoinColumn(name = "crypto_currency", foreignKey = @ForeignKey(name = "crypto_currency_ID_FK"))
    private CryptoCurrency cryptoCurrency;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private AppUser owner;

    @Builder
    public Account(String accountNumber, CryptoCurrency cryptoCurrency, AppUser owner) {
        super();
        this.accountNumber = accountNumber;
        this.cryptoCurrency = cryptoCurrency;
        this.owner = owner;
    }
}
