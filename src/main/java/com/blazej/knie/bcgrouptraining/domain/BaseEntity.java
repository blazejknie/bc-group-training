package com.blazej.knie.bcgrouptraining.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @Column(columnDefinition = "UUID")
    private UUID id = UUID.randomUUID();


    public BaseEntity(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
