package com.blazej.knie.bcgrouptraining.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@Entity
public class AppUser extends BaseEntity{
    private String firstName;
    private String lastName;
    private String mail;
    private String password;

    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @OneToMany(mappedBy = "owner")
    private Set<Account> accounts = new HashSet<>();

    @Builder
    public AppUser(String firstName, String lastName, String mail, String password,
                   RoleType roleType) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
        this.roleType = roleType;
    }

    public void update(AppUser appUser) {
        this.firstName = appUser.getFirstName();
        this.lastName = appUser.getLastName();
        this.mail = appUser.getMail();
        this.accounts = appUser.accounts;
    }

    public void addAccount(Account account) {
        this.accounts.add(account);
        account.setOwner(this);
    }
}
