package com.blazej.knie.bcgrouptraining.repositories;

import com.blazej.knie.bcgrouptraining.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AppUserRepository extends JpaRepository<AppUser, UUID> {
    Optional<AppUser> findByMail(String userMail);
}
