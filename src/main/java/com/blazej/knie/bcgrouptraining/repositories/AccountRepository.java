package com.blazej.knie.bcgrouptraining.repositories;

import com.blazej.knie.bcgrouptraining.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {
}
