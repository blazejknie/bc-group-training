package com.blazej.knie.bcgrouptraining.repositories;

import com.blazej.knie.bcgrouptraining.domain.CryptoCurrency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CryptoCurrencyRepository extends JpaRepository<CryptoCurrency, UUID> {
}
