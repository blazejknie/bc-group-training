package com.blazej.knie.bcgrouptraining.services;

import com.blazej.knie.bcgrouptraining.domain.AppUser;
import com.blazej.knie.bcgrouptraining.exceptions.AppUserNotFoundException;
import com.blazej.knie.bcgrouptraining.repositories.AppUserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AppUserService {

    private final AppUserRepository repository;

    public AppUserService(AppUserRepository repository) {
        this.repository = repository;
    }

    public AppUser findById(UUID userId) {
        return repository.findById(userId)
                .orElseThrow(() -> new AppUserNotFoundException(userId));
    }

    public List<AppUser> findAll() {
        return repository.findAll();
    }

    public AppUser save(AppUser user) {
        return repository.save(user);
    }

    public AppUser update(UUID userId, AppUser user) {
        return repository.findById(userId)
                .map(appUser -> {
                    appUser.update(user);
                    return appUser;
                })
                .orElseThrow(() -> new AppUserNotFoundException(userId));
    }

    public void deleteById(UUID userId) {
        if (!repository.existsById(userId)) {
            throw new AppUserNotFoundException(userId);
        }
        repository.deleteById(userId);
    }
}
