package com.blazej.knie.bcgrouptraining.controllers;

import com.blazej.knie.bcgrouptraining.domain.AppUser;
import com.blazej.knie.bcgrouptraining.services.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/")
public class AppUserController {

    private final AppUserService service;

    @GetMapping("app-users/{id}")
    public ResponseEntity<AppUser> fetchAppUserById(@PathVariable UUID id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @GetMapping("app-users")
    public ResponseEntity<List<AppUser>> fetchAllAppUsers() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping("app-users/{id}")
    public ResponseEntity<AppUser> updateAppUser(@PathVariable UUID id, @RequestBody AppUser appUser) {
        return new ResponseEntity<>(service.findById(id),HttpStatus.CREATED);
    }

    @DeleteMapping("app-users/{id}")
    public ResponseEntity<Void> deleteAppUserById(@PathVariable UUID id) {
        service.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
