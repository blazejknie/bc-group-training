package com.blazej.knie.bcgrouptraining.exceptions;

import java.util.UUID;

public class AppUserNotFoundException extends RuntimeException {
    public AppUserNotFoundException(UUID id) {
        super(String.format("Application User with id=%s does not exists!", id.toString()));
    }
}
