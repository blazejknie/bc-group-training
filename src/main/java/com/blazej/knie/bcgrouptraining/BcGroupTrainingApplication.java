package com.blazej.knie.bcgrouptraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication
public class BcGroupTrainingApplication {

    public static void main(String[] args) {
        SpringApplication.run(BcGroupTrainingApplication.class, args);
    }

}
