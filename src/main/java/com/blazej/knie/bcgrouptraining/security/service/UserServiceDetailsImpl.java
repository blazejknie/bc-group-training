package com.blazej.knie.bcgrouptraining.security.service;

import com.blazej.knie.bcgrouptraining.domain.AppUser;
import com.blazej.knie.bcgrouptraining.repositories.AppUserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceDetailsImpl implements UserDetailsService{

    private final AppUserRepository userRepository;

    public UserServiceDetailsImpl(AppUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userMail) throws UsernameNotFoundException {
        AppUser user = userRepository.findByMail(userMail)
                .orElseThrow(() -> new UsernameNotFoundException("invalid userMail"));
        return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(), getAuthority(user));
    }

    private List<SimpleGrantedAuthority> getAuthority(AppUser user) {
        return List.of(new SimpleGrantedAuthority(user.getRoleType().getRoleAsString()));
    }
}
