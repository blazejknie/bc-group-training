package com.blazej.knie.bcgrouptraining.seed;

import com.blazej.knie.bcgrouptraining.domain.Account;
import com.blazej.knie.bcgrouptraining.domain.AppUser;
import com.blazej.knie.bcgrouptraining.domain.CryptoCurrency;
import com.blazej.knie.bcgrouptraining.domain.RoleType;
import com.blazej.knie.bcgrouptraining.repositories.AccountRepository;
import com.blazej.knie.bcgrouptraining.repositories.AppUserRepository;
import com.blazej.knie.bcgrouptraining.repositories.CryptoCurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Service
public class SeedService implements CommandLineRunner {

    private final AppUserRepository appUserRepo;
    private final CryptoCurrencyRepository cryptoCurrencyRepo;
    private final AccountRepository accountRepo;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        AppUser appUser = AppUser.builder()
                .firstName("Adam")
                .lastName("Malinowski")
                .password(passwordEncoder.encode("adam123"))
                .roleType(RoleType.ADMIN)
                .mail("adam.malinowski@gmail.com")
                .build();
        appUserRepo.save(appUser);
        CryptoCurrency bitcoin = CryptoCurrency.builder()
                .name("Bitcoin")
                .valueInUSD(new BigDecimal("29793.67"))
                .build();
        CryptoCurrency savedCurrency = cryptoCurrencyRepo.save(bitcoin);
        Account adamAccount = Account.builder()
                .accountNumber("ACCOUNT NR")
                .cryptoCurrency(savedCurrency)
                .build();
        appUser.addAccount(adamAccount);
        accountRepo.save(adamAccount);
    }
}
